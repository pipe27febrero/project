import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
    @Get('')
    info() {
        return {
            name: 'University API', version: '1.0.0'
        }
    }
}