import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ConfigModule } from '@nestjs/config/dist/config.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { UniversitiesModule } from './universities/universities.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }),MongooseModule.forRootAsync({
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => {
      const uri = configService.get<string>('MONGO_URI')
      return { uri }
    }
  }), UniversitiesModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}