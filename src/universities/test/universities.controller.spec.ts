import { InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UniversityCreateDto } from '../dtos/university-create.dto';
import { UniversityUpdateDto } from '../dtos/university-update.dto';
import { UniversityDto } from '../dtos/university.dto';
import * as mapper from '../helpers/mapper';
import { IUniversity } from '../interfaces/university.interface';
import { UniversitiesController } from '../universities.controller';
import { UniversitiesService } from '../universities.service';

describe('UniversitiesController', () => {
  let universitiesController: UniversitiesController;
  let universitiesService: UniversitiesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UniversitiesController],
      providers: [{
        provide: UniversitiesService,
        useFactory: () => ({
          getAll: jest.fn(),
          getById: jest.fn(),
          create: jest.fn(),
          delete: jest.fn(),
          update: jest.fn()
        })
      }]
    }).compile();

    universitiesController = module.get<UniversitiesController>(UniversitiesController);
    universitiesService = module.get<UniversitiesService>(UniversitiesService)    
  });

  it('getAll',async () => {

    const universities : IUniversity[] = [{
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    },
    {
      _id: '5fbeada2393e50cfb58a51b6',
      name: 'University test 2',
      foundation: 1500,
      yearsAccredited: 7
    }]

    const mockResponse : UniversityDto[] = [{
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    },
    {
      _id: '5fbeada2393e50cfb58a51b6',
      name: 'University test 2',
      foundation: 1500,
      yearsAccredited: 7
    }]

    jest.spyOn(universitiesService,'getAll').mockResolvedValue(universities)
    jest.spyOn(mapper,'toUniversitiesDto').mockReturnValue(mockResponse)

    expect(await universitiesController.getAll()).toEqual(mockResponse)
    expect(universitiesService.getAll).toHaveBeenCalledTimes(1)
  })

  it('getById',async () => {

    const university : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    const mockResponse : UniversityDto = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    jest.spyOn(universitiesService,'getById').mockResolvedValue(university)
    jest.spyOn(mapper,'toUniversityDto').mockReturnValue(mockResponse)

    expect(await universitiesController.getById('5fbead6e3a1e66c73e0d70e1')).toEqual(mockResponse)
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith('5fbead6e3a1e66c73e0d70e1')
  })

  it('getById (Error)',async () => {

    jest.spyOn(universitiesService,'getById').mockResolvedValue(null)
    jest.spyOn(mapper,'toUniversityDto').mockReturnValue(null)

    try{
      await universitiesController.getById('5fbead6e3a1e66c73e0d7010')
    }
    catch(err)
    {
      expect(err).toBeInstanceOf(NotFoundException)
    }
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith('5fbead6e3a1e66c73e0d7010')
  })


  it('create',async() => {
    const universityCreateDto : UniversityCreateDto = {
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 7
    }
    const university : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d7020',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 7
    }

    const mockResponse : UniversityDto = {
      _id: '5fbead6e3a1e66c73e0d7020',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 7
    }

    jest.spyOn(universitiesService,'create').mockResolvedValue(university)
    jest.spyOn(mapper,'toUniversityDto').mockReturnValue(mockResponse)

    expect(await universitiesController.create(universityCreateDto)).toEqual(mockResponse)
    expect(universitiesService.create).toHaveBeenCalledTimes(1)
    expect(universitiesService.create).toHaveBeenCalledWith(universityCreateDto)
  })

  it('create (Error)',async() => {
    const universityCreateDto : UniversityCreateDto = {
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 7
    }

    jest.spyOn(universitiesService,'create').mockRejectedValue(`Database Error`)

    try{
      await universitiesController.create(universityCreateDto)
    }
    catch(err)
    {
      expect(err).toBeInstanceOf(InternalServerErrorException)
    }
    expect(universitiesService.create).toHaveBeenCalledTimes(1)
    expect(universitiesService.create).toHaveBeenCalledWith(universityCreateDto)
  })

  it('update',async () => {
    const universityId = '5fbead6e3a1e66c73e0d7020'
    const universityUpdateDto : UniversityUpdateDto= {
      name: 'University test updated',
      foundation: 1900,
      yearsAccredited: 1
    }
    const university : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d7020',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 7
    }

    const universityUpdated : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d7020',
      name: 'University test updated',
      foundation: 1900,
      yearsAccredited: 1
    }

    const mockResponse : UniversityDto= {
      _id: '5fbead6e3a1e66c73e0d7020',
      name: 'University test updated',
      foundation: 1900,
      yearsAccredited: 1
    }

    jest.spyOn(universitiesService,'getById').mockResolvedValue(university)
    jest.spyOn(universitiesService,'update').mockResolvedValue(universityUpdated)
    jest.spyOn(mapper,'toUniversityDto').mockReturnValue(mockResponse)

    expect(await universitiesController.update(universityUpdateDto,universityId)).toEqual(mockResponse)
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith(universityId)
    expect(universitiesService.update).toHaveBeenCalledWith(universityUpdateDto,universityId)
    expect(mapper.toUniversityDto).toHaveBeenCalledWith(universityUpdated)
  })

  it('update (Error)',async() => {
    const universityId = '5fbead6e3a1e66c73e0d7011'
    const universityUpdateDto : UniversityUpdateDto= {
      name: 'University test updated',
      foundation: 1900,
      yearsAccredited: 1
    }

    jest.spyOn(universitiesService,'getById').mockResolvedValue(null)

    try{
      await universitiesController.update(universityUpdateDto,universityId)
    }
    catch(err)
    {
      expect(err).toBeInstanceOf(NotFoundException)
    }
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith(universityId)
    expect(universitiesService.update).not.toHaveBeenCalled()
  })


  it('delete',async () => {
    const universityId = '5fbead6e3a1e66c73e0d70e1'

    const university : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    const mockResponse : UniversityDto = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    jest.spyOn(universitiesService,'getById').mockResolvedValue(university)
    jest.spyOn(universitiesService,'delete').mockResolvedValue(university)
    jest.spyOn(mapper,'toUniversityDto').mockReturnValue(mockResponse)

    expect(await universitiesController.delete(universityId)).toEqual(mockResponse)
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith(universityId)
    expect(universitiesService.delete).toHaveBeenCalledTimes(1)
    expect(universitiesService.delete).toHaveBeenCalledWith(universityId)
  })

  it('delete (Error)',async () => {
    const universityId = '5fbead6e3a1e66c73e0d7011'

    jest.spyOn(universitiesService,'getById').mockResolvedValue(null)

    try{
      await universitiesController.delete(universityId)
    }
    catch(err)
    {
      expect(err).toBeInstanceOf(NotFoundException)
    }
    expect(universitiesService.getById).toHaveBeenCalledTimes(1)
    expect(universitiesService.getById).toHaveBeenCalledWith(universityId)
    expect(universitiesService.delete).not.toHaveBeenCalled()
  })
});