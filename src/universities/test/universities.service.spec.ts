import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { UniversityCreateDto } from '../dtos/university-create.dto';
import { IUniversity } from '../interfaces/university.interface';
import * as mapper from '../helpers/mapper';
import { UniversityDocument } from '../schemas/university.schema';
import { UniversitiesService } from '../universities.service';

describe('UniversitiesService', () => {
  const university = {
    _id: '5fbead6e3a1e66c73e0d70e1',
    name: 'University test 1',
    foundation: 1950,
    yearsAccredited: 5
  }

  class UniversityModel{
    _id : string;
    name: string;
    foundation: number;
    yearsAccredited: number;

    constructor(u) {
      this._id = university._id
      this.name = u.name
      this.foundation = u.foundation
      this.yearsAccredited = u.yearsAccredited
    }
    save = jest.fn().mockResolvedValue(this)
    static findById = jest.fn()
    static remove = jest.fn()
    static find = jest.fn()
  }

  let universitiesService: UniversitiesService;
  let universityModel : Model<UniversityDocument>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UniversitiesService,{
        provide: getModelToken('University'),
        useValue: UniversityModel
      }],
    }).compile();

    universitiesService = module.get<UniversitiesService>(UniversitiesService);
    universityModel = module.get<Model<UniversityDocument>>(getModelToken('University'))
  });

  it('getAll',async () => {
    const mockResponse : IUniversity[] = [{
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }]

    const universityDocument : UniversityDocument = new universityModel(university)
    jest.spyOn(universityModel,'find').mockResolvedValue([universityDocument])
    jest.spyOn(mapper,'toIUniversities').mockReturnValue(mockResponse)

    expect(await universitiesService.getAll()).toEqual(mockResponse)
    expect(universityModel.find).toBeCalledTimes(1)
    expect(mapper.toIUniversities).toBeCalledTimes(1)
  })

  it('getById',async () => {
    const universityId = '5fbead6e3a1e66c73e0d70e1'
    const mockResponse : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    const universityDocument : UniversityDocument = new universityModel(university)

    jest.spyOn(universityModel,'findById').mockResolvedValue(universityDocument)
    jest.spyOn(mapper,'toIUniversity').mockReturnValue(mockResponse)

    expect(await universitiesService.getById(universityId)).toEqual(mockResponse)
    expect(universityModel.findById).toHaveBeenCalledTimes(1)
    expect(universityModel.findById).toHaveBeenCalledWith(universityId)
    expect(mapper.toIUniversity).toHaveBeenCalledTimes(1)
  })

  it('create',async () => {
    const universityCreateDto : UniversityCreateDto = {
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    const mockResponse : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    jest.spyOn(mapper,'toIUniversity').mockClear()
    jest.spyOn(mapper,'toIUniversity').mockReturnValue(mockResponse)

    expect(await universitiesService.create(universityCreateDto)).toEqual(mockResponse)
    expect(mapper.toIUniversity).toHaveBeenCalledTimes(1)
  })

  it('update',async () => {
    const universityId = '5fbead6e3a1e66c73e0d70e1'
    const universityUpdateDto  = {
      name: 'University test 2',
      foundation: 1900,
      yearsAccredited: 1
    }
    const mockResponse : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 2',
      foundation: 1900,
      yearsAccredited: 1
    }

    const universityDocument : UniversityDocument = new universityModel(university)

    jest.spyOn(mapper,'toIUniversity').mockClear()
    jest.spyOn(universityModel,'findById').mockClear()
    jest.spyOn(mapper,'toIUniversity').mockReturnValue(mockResponse)
    jest.spyOn(universityModel,'findById').mockResolvedValue(universityDocument)

    expect(await universitiesService.update(universityUpdateDto,universityId)).toEqual(mockResponse)
    expect(universityModel.findById).toHaveBeenCalledTimes(1)
    expect(universityModel.findById).toHaveBeenCalledWith(universityId)
    expect(mapper.toIUniversity).toHaveBeenCalledTimes(1)
  })

  it('delete',async () => {
    const universityId = '5fbead6e3a1e66c73e0d70e1'
    const mockResponse : IUniversity = {
      _id: '5fbead6e3a1e66c73e0d70e1',
      name: 'University test 1',
      foundation: 1950,
      yearsAccredited: 5
    }

    const universityDocument = new universityModel(university)

    jest.spyOn(universityModel,'findById').mockClear()
    jest.spyOn(mapper,'toIUniversity').mockClear()
    jest.spyOn(universityModel,'findById').mockResolvedValue(universityDocument)
    jest.spyOn(mapper,'toIUniversity').mockReturnValue(mockResponse)

    expect(await universitiesService.delete(universityId)).toEqual(mockResponse)
    expect(universityModel.remove).toHaveBeenCalledTimes(1)
    expect(universityModel.remove).toHaveBeenCalledWith(universityDocument)
    expect(mapper.toIUniversity).toHaveBeenCalledTimes(1)
  })

  it('delete (Error)',async () => {
    const universityId = '5fbead6e3a1e66c73e0d70e1'

    const universityDocument : UniversityDocument = null

    jest.spyOn(universityModel,'findById').mockClear()
    jest.spyOn(universityModel,'remove').mockClear()
    jest.spyOn(mapper,'toIUniversity').mockClear()
    jest.spyOn(universityModel,'findById').mockResolvedValue(universityDocument)

    try{
      await universitiesService.delete(universityId)
    }
    catch(err)
    {
      expect(err).toBeInstanceOf(Error)
    }

    expect(universityModel.findById).toHaveBeenCalledTimes(1)
    expect(universityModel.findById).toHaveBeenCalledWith(universityId)
    expect(universityModel.remove).not.toBeCalled()
    expect(mapper.toIUniversity).not.toBeCalled()
  })

});
