import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

export type UniversityDocument = University & Document;

@Schema()
export class University{
    @Prop()
    name: string;
    @Prop()
    foundation: number;
    @Prop()
    yearsAccredited: number;
}

export const UniversitySchema = SchemaFactory.createForClass(University)