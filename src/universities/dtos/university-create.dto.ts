export class UniversityCreateDto {
    readonly name: string;
    readonly foundation: number;
    readonly yearsAccredited: number;
}