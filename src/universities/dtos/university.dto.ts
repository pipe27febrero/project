export class UniversityDto {
    readonly _id: string;
    readonly name: string;
    readonly foundation: number;
    readonly yearsAccredited: number;
}