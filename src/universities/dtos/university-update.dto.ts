export class UniversityUpdateDto {
    readonly name?: string;
    readonly foundation?: number;
    readonly yearsAccredited?: number;
}