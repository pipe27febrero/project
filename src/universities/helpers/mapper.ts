import { UniversityDto } from "../dtos/university.dto";
import { IUniversity } from "../interfaces/university.interface";
import { UniversityDocument } from "../schemas/university.schema";

export const toUniversityDto = (university : IUniversity) : UniversityDto => {
    const {_id, name, foundation, yearsAccredited} = university
    const universityDto : UniversityDto =  {
        _id,
        name,
        foundation,
        yearsAccredited
    }
    return universityDto
}

export const toUniversitiesDto = (universities : IUniversity[]) : UniversityDto[] => {
    const universitiesDto = universities.map(university => toUniversityDto(university))
    return universitiesDto
}

export const toIUniversity = (university : UniversityDocument) : IUniversity => {
    const {_id, name, foundation, yearsAccredited} = university
    const universityDto : UniversityDto =  {
        _id,
        name,
        foundation,
        yearsAccredited
    }
    return universityDto
}

export const toIUniversities = (universities : UniversityDocument[]) : IUniversity[] => {
    const Iuniversities = universities.map(university => toIUniversity(university))
    return Iuniversities
}

