import { Body, Controller, Delete, Get, InternalServerErrorException, NotFoundException, Param, Post, Put } from '@nestjs/common';
import { UniversityCreateDto } from './dtos/university-create.dto';
import { UniversityUpdateDto } from './dtos/university-update.dto';
import { UniversityDto } from './dtos/university.dto';
import { toUniversitiesDto, toUniversityDto } from './helpers/mapper';
import { UniversitiesService } from './universities.service';

@Controller('universities')
export class UniversitiesController {
    constructor(private readonly universitiesService : UniversitiesService){}
    @Get()
    async getAll() : Promise<UniversityDto[]>{
        const universities = await this.universitiesService.getAll()
        const universitiesDto = toUniversitiesDto(universities) 
        return universitiesDto
    }

    @Get(':id')
    async getById(@Param('id') id : string) : Promise<UniversityDto>
    {
        const university = await this.universitiesService.getById(id)
        if(!university)
        {
            throw new NotFoundException(`Univeristy with id ${id} not found`)
        }
        const universityDto = toUniversityDto(university)
        return universityDto
    }   

    @Post()
    async create(@Body() universityCreateDto : UniversityCreateDto) : Promise<UniversityDto>
    {
        let universityCreated = null
        try{
            universityCreated = await this.universitiesService.create(universityCreateDto)
        }
        catch(err)
        {
            throw new InternalServerErrorException(err)
        }
        const universityDto = toUniversityDto(universityCreated)
        return universityDto
    }

    @Put(':id')
    async update(@Body() universityUpdateDto: UniversityUpdateDto,@Param('id') id : string) : Promise<UniversityDto>
    {
        const university = await this.universitiesService.getById(id)
        if(!university)
        {
            throw new NotFoundException(`Univeristy with id ${id} not found`)
        }
        const universityUpdated = await this.universitiesService.update(universityUpdateDto,id)
        const universityDto = toUniversityDto(universityUpdated)
        return universityDto
    }

    @Delete(':id')
    async delete(@Param('id') id : string) : Promise<UniversityDto>
    {
        const university = await this.universitiesService.getById(id)
        if(!university)
        {
            throw new NotFoundException(`Univeristy with id ${id} not found`)
        }
        await this.universitiesService.delete(id)
        const universityDto = toUniversityDto(university)
        return universityDto
    }
}
