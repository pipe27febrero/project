export interface IUniversity{
    _id: string;
    name: string;
    foundation: number;
    yearsAccredited: number;
}