import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UniversityCreateDto } from './dtos/university-create.dto';
import { UniversityUpdateDto } from './dtos/university-update.dto';
import { toIUniversities, toIUniversity } from './helpers/mapper';
import { IUniversity } from './interfaces/university.interface';
import { University, UniversityDocument } from './schemas/university.schema';

@Injectable()
export class UniversitiesService {
    constructor(@InjectModel(University.name) private universityModel: Model<UniversityDocument>){}
    async getAll() : Promise<IUniversity[]>{
        const universities : Array<UniversityDocument> = await this.universityModel.find()
        const response = toIUniversities(universities)
        return response
    }

    async getById(id: string) : Promise<IUniversity>
    {
        const university = await this.universityModel.findById(id)
        const response = toIUniversity(university)
        return response
    }

    async create(universityCreateDto : UniversityCreateDto) : Promise<IUniversity>{
       let createdUniversity = new this.universityModel(universityCreateDto)
       
       try{
           createdUniversity = await createdUniversity.save()
       }
       catch(err)
       {
            throw(new Error(err.toString()))
       }
       const response = toIUniversity(createdUniversity)
       return response
    }

    async delete(id: string) : Promise<IUniversity>{
        const university = await this.universityModel.findById(id)
        if(!university)
        {
            throw new Error(`University with id ${id} not found`)
        }
        await this.universityModel.remove(university)
        const response = toIUniversity(university)
        return response
    }

    async update(universityUpdateDto : UniversityUpdateDto,id: string) : Promise<IUniversity>{
        const university = await this.universityModel.findById(id)
        if(!university)
        {
            throw new Error(`University with id ${id} not found`)
        }
        university.foundation = universityUpdateDto.foundation !== undefined && universityUpdateDto.foundation >= 0 ? universityUpdateDto.foundation : university.foundation
        university.name = universityUpdateDto.name ? universityUpdateDto.name : university.name
        university.yearsAccredited = universityUpdateDto.yearsAccredited!== undefined && universityUpdateDto.yearsAccredited >= 0 ? universityUpdateDto.yearsAccredited : university.yearsAccredited
        try{
            await university.save()
        }
        catch(err)
        {
            throw(new Error(err.toString()))
        }
        const response = toIUniversity(university)
        return response
    }
}
