## Description

Basic Crud project

## Installation

```bash
$ npm install

```
## Configuring .env file
Create .env file and configure following variables
please put your mongodb uri for example:
```bash
 MONGO_URI=mongodb://localhost/project
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```


## Run with docker-compose 
add mongodb variable in .env file
```
MONGO_URI=mongodb://mongo/project
```
and run by the following command
```
docker-compose up -d --build
```